var express = require('express'),
  app = express(),
 bodyParser = require('body-parser'),
  cors = require('cors');

app.use(cors());
app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({extended: true,limit: '5mb'}));
app.use(express.static(__dirname + '/../../client/dist'));

var empList = [
  {
    name:"Akhil1",
    empId:65365341,
    salary:576571,
    bu:"CCBU1"
  },
  {
    name:"Akhil2",
    empId:65365342,
    salary:576572,
    bu:"CCBU2"
  },
  {
    name:"Akhil3",
    empId:65365343,
    salary:576573,
    bu:"CCBU3"
  },
  {
    name:"Akhil4",
    empId:65365344,
    salary:576574,
    bu:"CCBU4"
  }
];

app.get('/employee-list',function (req,res,next) {
  res.send(empList);
});

app.post("/add-employee",function (req,res,next) {
  empList.push(req.body.employee);
  res.send(empList);
});

app.listen(3200, function() {
  console.log('listening on port : ' + 3200);
});
