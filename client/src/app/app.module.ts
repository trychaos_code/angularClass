import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {AppRoutingModule} from './app-routing'

import {Game} from './game/app.component'
import {Calculator} from './calculater/app.component'
import {Employee} from './employee/app.component'

@NgModule({
  declarations: [
    AppComponent, Game, Calculator, Employee
  ],
  imports: [
    BrowserModule, FormsModule, AppRoutingModule, HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
