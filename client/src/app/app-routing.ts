import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {Game} from './game/app.component';
import {Calculator} from './calculater/app.component'
import {Employee} from './employee/app.component'

const routes: Routes = [
  { path: '', redirectTo: '/game', pathMatch: 'full' },
  { path: 'game',  component: Game },
  { path: 'calc',  component: Calculator },
  { path: 'emp',  component: Employee }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {


}
